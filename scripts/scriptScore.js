// Checks if html page is the right page
if (document.URL.includes("scorePage.html") === true) {
    document.getElementById("totalScore").innerHTML = "Totaal aantal haalbare punten: 87";
    document.getElementById("score").innerHTML = "U heeft behaald: " + sessionStorage.getItem("totalPoints");
    document.getElementById("answers").innerHTML = answerScreen();
}

// Count & stores the total points and store the answer
function countRoundPoints(biasAnswer, measureAnswer, biasPoints, measurePoints) {
    // sets good or false answer
    let checkBias = checkPoints(biasPoints);
    let checkMeasure = checkPoints(measurePoints);
    // Write down the message to store
    let round = sessionStorage.getItem("round");
    let answerBias = "Ronde " + round + " Bias: Antwoord " + biasAnswer + " is " + checkBias + " beantwoord.";
    let answerMeasure = "Ronde " + round + " Maatregel: Antwoord " + measureAnswer + " is " + checkMeasure + " beantwoord.";
    // Store in sessionStorage
    let biasKey = "answerBias" + round
    sessionStorage.setItem(biasKey, answerBias);
    let measureKey = "answerMeasure" + round
    sessionStorage.setItem(measureKey, answerMeasure);

    return biasPoints + measurePoints + canvasPoints;
}

// Check if answer is good or false
function checkPoints(points) {
    if (points === 5 ){
        return "goed";
    } else {
        return  "fout";
    }
}

// Display all answers
function answerScreen() {
    let round1Bias = sessionStorage.getItem("answerBias1");
    let round1Measure = sessionStorage.getItem("answerMeasure1");
    let round2Bias = sessionStorage.getItem("answerBias2");
    let round2Measure = sessionStorage.getItem("answerMeasure2");
    let round3Bias = sessionStorage.getItem("answerBias3");
    let round3Measure = sessionStorage.getItem("answerMeasure3");
    let round4Bias = sessionStorage.getItem("answerBias4");
    let round4Measure = sessionStorage.getItem("answerMeasure4");
    let round5Bias = sessionStorage.getItem("answerBias5");
    let round5Measure = sessionStorage.getItem("answerMeasure5");
    let round6Bias = sessionStorage.getItem("answerBias6");
    let round6Measure = sessionStorage.getItem("answerMeasure6");

    return round1Bias + "<br/>" + round1Measure + "<br/>" + "<br/>" +
        round2Bias + "<br/>" + round2Measure + "<br/>" + "<br/>" +
        round3Bias + "<br/>" + round3Measure + "<br/>" + "<br/>" +
        round4Bias + "<br/>" + round4Measure + "<br/>" + "<br/>" +
        round5Bias + "<br/>" + round5Measure + "<br/>" + "<br/>" +
        round6Bias + "<br/>" + round6Measure + "<br/>";
}
document.getElementById("login-button").addEventListener("click", buttonLoginClick);

// Button that brings you to welcome page
function buttonLoginClick() {
    let input = document.getElementById("name").value;
    sessionStorage.setItem("name", input);
    window.open("html/welcome.html", '_top');
}

// Makes sure on submit to go to the next page (Also makes sure the enter key works in a neat way)
document.getElementById("form").addEventListener('submit', function (event){
    event.preventDefault();
    buttonLoginClick();
});

let timerValue;
let canvasPoints;
// Collections
let biasCollection;
let measureCollection;
let newsArticleCollection;
let metricCollection;
let canvassesCollection;

var refresh = false;

function initialize(output) {
    // Initialize variables
    let canvasNumber = parseInt(sessionStorage.getItem("canvasNumber"));
    let round = sessionStorage.getItem("round");
    timerValue = parseInt(sessionStorage.getItem("timerValue"));
    canvasPoints = output.canvases[canvasNumber].points;
    // Initialize collection
    biasCollection = output.scenario.biasQuestions;
    canvassesCollection = output.canvases;
    measureCollection = output.scenario.measureQuestions;
    newsArticleCollection = canvassesCollection[canvasNumber].newsArticles;
    metricCollection = [canvassesCollection[canvasNumber].value1, canvassesCollection[canvasNumber].value2, canvassesCollection[canvasNumber].value3];
    // Extra clear of data
    document.getElementById("all-bias-div").innerHTML = "";
    document.getElementById("bias-table-div").innerHTML = "";
    resetTimer();
    //Will change the map & news articles for the next round
    let canvasNumberCorrection = canvasNumber + 1
    document.getElementById("map-img").src = "../images/game/R" + round + "C" + canvasNumberCorrection + ".png";
    popupNewsfeed(newsArticleCollection);

    loadGame(output, round)
}

function loadGame(output, round) {
        let roundTitle = "Ronde " + output.roundNumber + ": "
        loadRoundScenario(roundTitle, output.title, output.scenario.title, output.scenario.text);
        loadCollections();
        fetchBiases();
        styleStart();
        setOpenCloseModal();
        startRoundModal(round);
        refreshBrowser(output.timer.name);
        // Loads the biases in the questionmark button (called in scriptBiasTable.js)
        biasTable(biasCollection, document.getElementById('bias-table-div'));
}

// THESE ITEMS SET STARTING STYLING
function styleStart() {
    // Start display none
    document.getElementById("scenario-box").style.display = 'none';
    document.getElementById("question-box").style.display = 'none';
    document.getElementById("infection-box").style.display = 'none';
    // Animate start screen
    document.getElementById("canvas-footer").style.animation = "fadeIn 3s";
    document.getElementById("game-container").style.animation = "fadeIn 3s"
    document.getElementById("scenario-box").style.animation = "fadeIn 2s";
    document.getElementById("question-box").style.animation = "fadeIn 2s";
    // Will make the BIAS question default showable
    document.getElementById("qBiasId").click();
}

// THESE ITEMS LOADS DATA
// Changes the title, scenario & text to what is send into the function
function loadRoundScenario(round, title, scenarioTitle, scenarioText) {
    document.getElementById("title-round").innerHTML = round + title;
    document.getElementById("scenario-title").innerHTML = scenarioTitle;
    document.getElementById("scenario-text").innerHTML = scenarioText;
}

// Changes the biases & the measurements in the question tab
function loadCollections() {
    // Bias collection
    document.getElementById("biasA").nextElementSibling.innerHTML = biasCollection[0].bias.name;
    document.getElementById("biasB").nextElementSibling.innerHTML = biasCollection[1].bias.name;
    document.getElementById("biasC").nextElementSibling.innerHTML = biasCollection[2].bias.name;
    // Measure Collection
    document.getElementById("measureA").nextElementSibling.innerHTML = measureCollection[0].measure.answer;
    document.getElementById("measureB").nextElementSibling.innerHTML = measureCollection[1].measure.answer;
    document.getElementById("measureC").nextElementSibling.innerHTML = measureCollection[2].measure.answer;
    // Infection Collection
    document.getElementById("healthy-population").innerHTML = metricCollection[0];
    document.getElementById("infected-population").innerHTML = metricCollection[1];
    document.getElementById("mutated-population").innerHTML = metricCollection[2];
    // NewsArticles Collection
    // Newsfeed goes in a loop to get ALL articles needed for the canvas (can be 2 or 3)
    for (let i = 0; i < newsArticleCollection.length; i++) {
        loadNewsfeed(i, newsArticleCollection[i].title, newsArticleCollection[i].message, newsArticleCollection[i].source);
    }
}

// CANVAS FUNCTIONS
// Button that brings you to the login page & alerts you goodbye
function buttonLogoutClick() {
    if (sessionStorage.getItem("round") >= 7) {
        sessionStorage.clear();
        window.open('../index.html', '_top');
        return;
    }
    logoutModal();
}

// Called on body load of canvas.html. When pressing the backbutton from the scorepage buttonLogoutClick gets called
function resetGame() {
    if (sessionStorage.getItem("round") >= 7) {
        sessionStorage.clear();
        window.open('index.html', '_top');
    }
}

// Detect if page gets reloaded
function refreshBrowser(minutes) {
    refresh = true;
    if (sessionStorage.getItem("oldCanvasNumber") !== sessionStorage.getItem("canvasNumber")) {
        sessionStorage.setItem("oldCanvasNumber", sessionStorage.getItem("canvasNumber"));
        refresh = false;
    }
    // set new timer value when page gets reloaded
    if (refresh === false) {
        timerValue = minutes * 60000;
    }
}
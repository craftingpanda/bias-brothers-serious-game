function popupNewsfeed(newsArticleCollection) {
    document.getElementById("newsfeed-modal").style.display = "none";
    for (let i = 0; i < newsArticleCollection.length; i++) {
        if (newsArticleCollection[i].popUp === true) {
            document.getElementById("popup-title").innerHTML = newsArticleCollection[i].title;
            document.getElementById("popup-text").innerHTML = newsArticleCollection[i].message;
            document.getElementById("popup-paper").innerHTML = newsArticleCollection[i].source;
            break;
        }
    }
}

// Changes the articles max of 3 (CHECK IF WE COULD DO MORE)
function loadNewsfeed(articleNumber, title, message, source) {
    articleNumber++;
    document.getElementById("title-" + articleNumber).innerHTML = title;
    document.getElementById("message-" + articleNumber).innerHTML = message;
    document.getElementById("source-" + articleNumber).innerHTML = source;
}

window.addEventListener("click", function (event) {
    if (event.target === this.document.getElementById("newsfeed-button")) {
        if (document.getElementById('newsfeed').style.display === "block") {
            document.getElementById('newsfeed').style.display = "none"
        } else {
            document.getElementById('newsfeed').style.display = "block";
        }
    }
    else {
        document.getElementById('newsfeed').style.display = "none";
    }
})
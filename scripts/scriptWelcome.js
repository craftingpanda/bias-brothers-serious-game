let input = sessionStorage.getItem('name');

document.getElementById("welcome-text").innerHTML = "Welkom " +
    input + ", bij de Serious Game!";

document.getElementById("welcome-button").addEventListener("click", gameEnvironment);

// set start round session storage
sessionStorage.setItem("round", "1");
sessionStorage.setItem("canvasNumber", "2");
sessionStorage.setItem("totalPoints", "0");

// Button that brings you to the game page & alerts you welcome
function gameEnvironment() {
    window.open("canvas.html", '_top');
}
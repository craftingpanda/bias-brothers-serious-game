/*
---HOW TO USE---
Call openCloseModal in setOpenCloseModal
Give button, modal & index for the closing button
Index might need some testing to see wich one it is.

Underneath you can place dialoge for specific modal
 */
// Function to set up modals so that it can open and close
function openCloseModal(button, modal, index) {
    // Get the <closeModal> element that closeModals the biasModal
    let closeModal = document.getElementsByClassName("closeModal")[index];
    // When the user clicks on the button, open the biasModal
    button.onclick = function () {
        modal.style.display = "block";
    }
    // When the user clicks on <closeModal> (x), the modal will close
    closeModal.onclick = function () {
        modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the biasModal, the modal will close
    window.addEventListener("click", function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    })
}

// Here you can add new popup window by calling openCloseModal
function setOpenCloseModal(){
    openCloseModal(document.getElementById("next"), document.getElementById("warningModal"), 0);
    openCloseModal(document.getElementById("qMeasureId"), document.getElementById("warningModal"), 0);
    openCloseModal(document.getElementById("questionmark-img"), document.getElementById("biasModal"), 5);
    openCloseModal(document.getElementById("allBiasesBtn"), document.getElementById("allBiasesModal"), 3);
    openCloseModal(document.getElementById("informationBtn"), document.getElementById("informationModal"), 2);
}

// Displayed text for every round
function startRoundModal(round) {
    document.getElementById("round-message").innerHTML = "Welkom in ronde " + round + ".\n Maak je snel klaar voor de start!";
    document.getElementById("roundModal").style.display = "block";
}

// Displayed text for logging out
function logoutModal() {
    document.getElementById("logout-modal").style.display = "block";
    document.getElementById("yes").addEventListener("click", function() {
        sessionStorage.clear();
        window.open('../index.html', '_top');
    });
    document.getElementById("no").addEventListener("click", function() {
        document.getElementById("logout-modal").style.display = "none";
    });
    document.getElementsByClassName("closePopup")[2].onclick = function () {
        document.getElementById("logout-modal").style.display = "none";
    }
}
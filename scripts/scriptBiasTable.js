// creates element by giving name & tag
function createElement(name, tag) {
    let element = document.createElement(tag);
    element.innerHTML = name;

    return element
}

// Creates both bias tables in HTML
function biasTable(list, div) {
    let table = document.createElement("table");
    let thead = document.createElement('thead');
    let tbody = document.createElement('tbody');

    table.appendChild(thead);
    table.appendChild(tbody);

    // Adding the entire table to the body tag
    div.appendChild(table);

    // Creating and adding data to first row of the table
    let row_1 = document.createElement('tr');

    row_1.appendChild(createElement("Bias naam", "th"));
    row_1.appendChild(createElement("Omschrijving", "th"));
    row_1.appendChild(createElement("Voorbeeld", "th"));
    thead.appendChild(row_1);

    // Looping through all biases that are passed in
    for (let i = 0; i < list.length; i++) {
        let row = document.createElement('tr')

        if (list[i].name === undefined) {
            row.appendChild(createElement(list[i].bias.name, "td"));
            row.appendChild(createElement(list[i].bias.description, "td"));
            row.appendChild(createElement(list[i].bias.example, "td"));

        } else {
            row.appendChild(createElement(list[i].name, "td"));
            row.appendChild(createElement(list[i].description, "td"));
            row.appendChild(createElement(list[i].example, "td"));
        }
        tbody.appendChild(row);

        // Gives a grey color to a row if the number is uneven
        if (i % 2 === 0) {
            row.style.backgroundColor = 'rgba(128, 128, 128, 0.212)';
        }
    }
}
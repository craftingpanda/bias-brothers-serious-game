// Checks if both questions have been filled in.
document.getElementById("next").addEventListener
    ("click", giveAnswer);
function giveAnswer() {
    let bias = document.getElementsByName('answer-bias');
    let measure = document.getElementsByName('answer-measure');
    let biasValue;
    let measureValue;
    let biasNumber;
    let measureNumber;
    let biasPoints
    let measurePoints

    for (let i = 0; i < bias.length; i++) {
        if (bias[i].checked) {
            biasValue = bias[i].value;
            biasNumber = i;
            biasPoints = biasCollection[i].points;
        }
    }

    for (let j = 0; j < measure.length; j++) {
        if (measure[j].checked) {
            measureValue = measure[j].value;
            measureNumber = j;
            measurePoints = measureCollection[j].points;
        }
    }

    if (biasValue === undefined || measureValue === undefined) {
        //alert("U heeft niet allebei de vragen ingevuld.")
        document.getElementById("warning-message").innerHTML = "U heeft niet allebei de vragen ingevuld.";
    } else {
        nextRound(biasValue, measureValue, measurePoints, biasPoints);
        bias[biasNumber].checked = false;
        measure[measureNumber].checked = false;
        document.getElementById("next").onclick = function () {
            document.getElementById("warningModal").style.display = "none";
        }
    }
}

// Checks the answers and loads the correct new round json file.
function nextRound(biasAnswer, measureAnswer, measurePoints, biasPoints) {
    let round = parseInt(sessionStorage.getItem("round"));
    let canvasNumber = parseInt(sessionStorage.getItem("canvasNumber"));
    let roundPoints = countRoundPoints(biasAnswer, measureAnswer, biasPoints, measurePoints);
    let newTotalPoints = parseInt(sessionStorage.getItem("totalPoints")) + roundPoints;
    // Store new values
    fetchNewCanvasNumber(canvasNumber, measurePoints);
    round = round + 1;
    sessionStorage.setItem("round", round.toString());
    sessionStorage.setItem("totalPoints", newTotalPoints);
    sessionStorage.setItem("timerValue", timerValue);
    // Extra timer clear
    clearInterval(x);
    document.getElementById("timer").style.backgroundColor = "rgba(97,206,112,0)";
    document.getElementById("timer").innerHTML = "";
    // GET new round data & load new game
    fetchRound();
}


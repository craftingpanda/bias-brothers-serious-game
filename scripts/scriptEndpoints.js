const gameNumber = 1;
let output;

// Gets all the biases from the backend with an http request. Backend started with intelliJ.
function fetchBiases() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:8080/bias', true);
    xhr.onload = function () {
        if (this.status === 200) {
            const output = JSON.parse(this.responseText);
            biasTable(output, document.getElementById("all-bias-div"));
        } else {
            if (this.status === 500) {
                console.log("500 status");
                fetchBiases();
            }
        }
    }
    xhr.send();
}

function fetchRound() {
    let totalRounds = 7;
    let round = parseInt(sessionStorage.getItem("round"));
    if (round === totalRounds)
    {
        window.open("scorePage.html", '_top');
    } else {
        let xhr = new XMLHttpRequest();
        let url = "http://localhost:8080/round/" + sessionStorage.getItem("round");
        xhr.open('GET', url, true);
        xhr.onload = function () {
            if (this.status === 200) {
                output = JSON.parse(this.responseText);
                initialize(output);
            } else {
                if (this.status === 500) {
                    console.log("500 status");
                    fetchRound();
                }
            }
        }
        xhr.send();
    }
}

function fetchNewCanvasNumber(canvasNumber, measurePoints) {
    let round = parseInt(sessionStorage.getItem("round"));
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:8080/checkAnswer/' + round + "/" + canvasNumber + "/" + measurePoints, true);
    xhr.onload = function () {
        if (this.status === 200) {
            const output = JSON.parse(this.responseText);
            sessionStorage.setItem("canvasNumber", output)
        } else {
            if (this.status === 500) {
                console.log("500 status");
                fetchNewCanvasNumber(canvasNumber, measurePoints);
            }
        }
    }
    xhr.send();
}

// Start first canvas
fetchRound();